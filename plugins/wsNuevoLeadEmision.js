import axios from 'axios'

const promoCore = process.env.promoCore


const newLeadEmision = {}

newLeadEmision.newLeadEm = function (peticion, accessToken) {

  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
    url: promoCore + '/v3/issue/request_online',
    data: JSON.parse(peticion)
  })
}
export default newLeadEmision


