import axios from 'axios'

// let catalogo = process.env.catalogo + "/catalogos";
let urlConsumo = process.env.catalogosDirectosGNP +  '/gnp_catalogos_motos';


class Catalogos {
  modelos(marca) {
    return axios({
      method: "get",
      url: urlConsumo + `/modelos_motos?marca=${marca}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    })
  }
  submarcas(marca, modelo) {
    return axios({
      method: "get",
      url: urlConsumo + `/submarcas_motos?marca=${marca}&modelo=${modelo}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    })
  }
  descripciones(marca, modelo, submarca) {
    return axios({
      method: "get",
      url: urlConsumo + `/descripciones_motos?marca=${marca}&modelo=${modelo}&submarca=${submarca}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    })
  }
}

export default Catalogos;
