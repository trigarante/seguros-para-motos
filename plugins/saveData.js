import axios from "axios";
import configDB from "./configBase";

const promoCore = process.env.promoCore

const cotizacionService = {};

cotizacionService.search = function(
  nombreUsr,
  telefonoUsr,
  emailUsr,
  generoUsr,
  codigoPostalUsr,
  edadUsr,
  datosCot,
  respuestaCot,
  emailValid,
  telefonoValid,
  mensajeSMS,
  codigoPostalValid,
  idSubRamo,
  idMedioDifusion,
  aseguradora,
  idPagina,
  ip,
  telefonoAS,
  accessToken
) {
  var u_r_l = promoCore + '/v1/motorcycle';
  return axios({
    method: "post",
    headers: { Authorization: `Bearer ${accessToken}` },
    url: u_r_l,
    data: {
      nombreUsr,
      telefonoUsr,
      emailUsr,
      generoUsr,
      codigoPostalUsr,
      edadUsr,
      datosCot,
      respuestaCot,
      emailValid,
      telefonoValid,
      mensajeSMS,
      codigoPostalValid,
      idSubRamo,
      idMedioDifusion,
      aseguradora,
      idPagina,
      ip,
      telefonoAS,
    },
  });
};
export default cotizacionService;
