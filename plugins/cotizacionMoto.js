import axios from 'axios'
import configDB from './configBase'

const promoCore = process.env.promoCore

const cotizacionService = {}

cotizacionService.search = function (aseguradora,peticion,accessToken) {

 switch (aseguradora) {
  case 'GNP':
     return axios({
       method: "post",
       headers: { Authorization: `Bearer ${accessToken}` },
       url: promoCore + `/v2/gnp/motorcycle/quotation`,
       data: JSON.parse(peticion)
     })
    break;
 
  default:
     return axios({
       method: "post",
       headers: { Authorization: `Bearer ${accessToken}` },
       url: promoCore + `/v2/qualitas/motorcycle/quotation`,
       data: JSON.parse(peticion)
     })
    break;
 }
}
export default cotizacionService
