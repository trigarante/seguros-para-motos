import autosService from './ws-autos'

const modelosService ={}

modelosService.search=function (marca, accessToken) {
  return autosService.get(`/motos/gnp/modelos?marca=${marca}`,{
    headers: { Authorization: `Bearer ${accessToken}` },
  })
}
export default modelosService
