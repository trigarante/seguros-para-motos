const conversion ={}

conversion.gtag_report_conversion=function(url) {
  var callback = function () {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  };
  gtag('event', 'conversion', {
    'send_to': 'AW-951531955/2_pkCNDz_lwQs_PcxQM',
    'event_callback': callback
  }); 
  return false;
}
export default conversion
