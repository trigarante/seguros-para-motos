import autosService from './ws-autos'

const subdescripcionesService ={}

subdescripcionesService.search=function (marca, modelo, submarca, accessToken) {
  return autosService.get(`/motos/gnp/descripciones?marca=${marca}&modelo=${modelo}&submarca=${submarca}`,{
    headers: { Authorization: `Bearer ${accessToken}` },
  })
}
export default subdescripcionesService
