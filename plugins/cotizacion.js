import axios from 'axios'
import configDB from './configBase'

const promoCore = process.env.promoCore

const cotizacionService = {}

cotizacionService.search = function (aseguradora,peticion,accessToken) {
  // debugger
  switch(aseguradora){
    case 'GNP':
      return axios({
        method: "post",
        headers: { Authorization: `Bearer ${accessToken}` },
        url: promoCore + `/v2/gnp/motorcycle/quotation`,
        data: JSON.parse(peticion)
      });
      break;
      case 'QUALITAS':
      
        return axios({
          method: "post",
          headers: { Authorization: `Bearer ${accessToken}` },
          url: promoCore + `/v2/qualitas/motorcycle/quotation`,
          data: JSON.parse(peticion)
        });
        
    }

  }


export default cotizacionService
