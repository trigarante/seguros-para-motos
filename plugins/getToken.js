import axios from 'axios'
import configDB from './configBase'

const promoCore = process.env.promoCore

const getTokenService = {}

getTokenService.search = function () {
    return axios({
        method: "post",
        url: promoCore + '/v1/authenticate',
        data: {"tokenData": configDB.tokenData}
    })
}
export default getTokenService
