import * as axios from 'axios'

const promoCore = process.env.promoCore

class validacionesService {

    validarCorreo(email) {
        return axios({
            method: "get",
            url: process.env.urlValidaciones + `/validacion/correo?email=`+email,
        });
    }

    validarTelefono(telefono) {
        return axios({
            method: "get",
            url: process.env.urlValidaciones + '/validacionTel/validacion?telefono='+telefono,
        })
    }

    validarCodigoPostal(cp, accessToken){
        return axios({
            method: "get",
            headers: {'Access-Control-Allow-Origin': '*',
                Authorization: `Bearer ${accessToken}`
            },
            url: promoCore + '/v2/sepomex/'+cp
        })
    }
}

export default validacionesService
