import axios from 'axios'

const promoCore = process.env.promoCore

const saveDataCliente = {}

saveDataCliente.search = function (data, accessToken) {
    return axios({
        method: "post",
        headers: { Authorization: `Bearer ${accessToken}` },
        url: promoCore + '/v2/json_quotation/customer_record',
        data: JSON.parse(data)
    })
}
export default saveDataCliente