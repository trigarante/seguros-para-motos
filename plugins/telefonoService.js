import axios from 'axios'

const promoCore = process.env.promoCore

const telApi = {}
telApi.search = function (idDiffusionMedium) {
    
    return axios({
        method: "get",
        url: promoCore + '/v1/page/diffusion-medium/phone?idDiffusionMedium='+idDiffusionMedium,    
    })
}
export default telApi
