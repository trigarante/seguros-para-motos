const promo = {
  all: [
    {
      aseguradorapromo: "MOTOS", //i
      promos: [
        {
          //j
          nombre: "NORMAL",
          dias: [
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25,
            26,
            27,
            28,
            29,
            30,
            31
          ], //Si en una promo de más abajo dentro de la misma aseguradorapromo se repite el mismo dia, se sobrescribirá.
          descuentoPagina: "20% de descuento", //descuentoPagina
          promoMsi: " + 3, 6, 9 y 12 " //descuentoPagina + promoPeTOP
        },
        {
          nombre: "ESPECIAL",
          dias: [13, 14, 15, 16, 17, 18, 19],
          descuentoPagina: "10% de descuento", //descuentoPagina
          promoMsi: " + 3, 6 y 12" //descuentoPagina + promoPeTOP
        }
      ]
    }
  ] //Fin all
};

export default promo;
