
import axios from 'axios'

// let catalogo = process.env.catalogo + "/catalogos";
// debugger
let urlConsumoQ = process.env.catalogosDirectosQualitas +  '/v3/qualitas-motos';
// console.log(urlConsumoQ)


class CatalogosQualitas {
  marcas() {
    return axios({
      method: "get",
      url: urlConsumoQ + `/brands`,
      headers: {'Access-Control-Allow-Origin': '*',},
    })    
  }
  modelos(marca) {
    return axios({
      method: "get",
      url: urlConsumoQ + `/years?brand=${marca}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    })
  }
  submarcas(marca, modelo) {
    return axios({
      method: "get",
      url: urlConsumoQ + `/models?brand=${marca}&year=${modelo}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    })
  }
  descripciones(marca, modelo, submarca) {
    return axios({
      method: "get",
      url: urlConsumoQ + `/variants?brand=${marca}&model=${submarca}&year=${modelo}`,
      headers: {'Access-Control-Allow-Origin': '*',},
    })
  }
}

export default CatalogosQualitas;
