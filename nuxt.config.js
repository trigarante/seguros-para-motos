module.exports = {
  target: "static",
  /*
   ** Headers of the page
   */
  head: {
    htmlAttrs: { lang: "es-MX" },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
    ],
    link: [
      {
        rel: "icon",
        type: "image/png",
        href: "/seguros-para-motos/img/favicon.png",
      },
    ],
  },
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#0065c5" },
  /*
   ** Build configuration
   */
  build: {
    // analyze:true,
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          // loader: "eslint-loader",
          exclude: /(node_modules)/,
        });
      }
    },
  },
  css: ["static/css/bootstrap.min.css", "static/css/styles.min.css"],
  modules: ["@nuxtjs/axios"],
  // { id: "GTM-KRCFZQ" }],

  plugins: [
    { src: "~/plugins/filters.js", ssr: false },
    { src: "~/plugins/apm-rum.js", ssr: false },
  ],
  env: {
    tokenData: "mHf/0x8xqWmYlrjaRWECOzmkksnuNDZv1fBvMLjpI2g=",
    Ahorra: "https://dev.core-ahorraseguros.com",
    catalogosDirectosGNP: "https://dev.web-gnp.mx",
    catalogosDirectosQualitas: "https://dev.ws-qualitas.com",
    urlValidaciones: "https://core-blacklist-service.com/rest",
    motorGNP: "https://p.gnp-comprasegura.com/cliente",
    motorQualitas: "https://p.qualitas-comprasegura.com/cliente",
    sitio: "https://p.ahorraseguros.mx",
    /*MONITOREO*/
    coreBranding: "https://dev.core-brandingservice.com", // CORE //en dev
    urlMonitoreo: "https://core-monitoreo-service.com/v1",
    promoCore: "https://dev.core-persistance-service.com", //CORE DESCUENTOS
    hubspot:"https://core-hubspot-dev.mark-43.net/deals/landing",
    Environment:'DEVELOP'
  },
  render: {
    http2: { push: true },
    resourceHints: false,
    compressor: { threshold: 9 },
  },
  router: {
    base: "/seguros-para-motos/",
  },
};
