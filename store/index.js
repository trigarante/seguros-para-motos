import Vuex from "vuex";
import database from "~/plugins/saveData";
import cotizarMoto from "~/plugins/cotizacion";
import getTokenService from "../plugins/getToken";
import validacionesService from "../plugins/validaciones";
import monitoreoService from "../plugins/monitoreo";
import dbService from "../plugins/configBase";
import promo from "~/plugins/promociones";
import cotizacionPromo from "~/plugins/descuentoService.js";
import telApi from "~/plugins/telefonoService.js";
import saveDataCliente from "~/plugins/saveDataCliente";
import saveHub from "~/plugins/saveHub";
import SaveService from "../plugins/saveTrigarante";

const createStore = () => {
  const validacion = new validacionesService();
  const saveService = new SaveService()
  return new Vuex.Store({
    state: {
      ambientePruebas: false,
      cargandocotizacion: false,
      msj: false,
      msjEje: false,
      cotizacionesAmplia: [],
      cotizacionesLimitada: [],
      tiempo_minimo: 1.3,
      sin_token: false,
      gclid_field: '',
      config: {
        time:"",
        btnTop: '/',
        cards: false,
        detalles: false,
        detallesGNP: false,
        aztecaGnp: false,
        combinado: false,
        telphone: false,
        telefonoAzteca:'25276765',
        telefonoQualitas:'25276765',
        telefonoMigo:'25276765',
        telefonoMapfre:'25276765',
        telefonoBanorte:'25276765',
        tresCaminos: false,
        principal: false,
        msjEje: false,
        imgen: "",
        urlFinal: "/thank-you",
        habilitarBtnInteraccion: true,
        habilitarBtnEmision: true,
        habilitarBtnInteraccion: false,
        btnEmisionDisabled: false,
        buttonPrincipal: true,
        tipoPaquete: 0,
        aseguradora: "",
        cotizacion: true,
        emision: true,
        descuento: 0,
        telefonoAS: "",
        grupoCallback: "VN GNPMOTOS",
        from: "DOMINIO-GNPMOTOS",
        idPagina: 0,
        idAseguradora: 0,
        accessToken: "",
        ipCliente: "",
        aseguradorapromo: "",
        urlImagen: "/seguros-para-motos",
        dto: "",
        msi: "",
        promoLabel: "",
        promoImg: "",
        promoSpecial: false,
        clase2: "col-4 col-sm-5 col-md-12 col-lg-4",
        clase3: "col-8 col-sm-7 col-md-12 col-lg-8 my-auto",
        idLog_cliente: ''
      },
      ejecutivo: {
        nombre: "",
        correo: "",
        id: 0
      },
      formData: {
        urlDestino: "",
        urlOrigen: "",
        aseguradora: "",
        marca: "",
        modelo: "",
        descripcion: "",
        detalle: "",
        clave: "",
        cp: "",
        nombre: "",
        telefono: "",
        correo: "",
        gclid_field: '',
        edad: "",
        fechaNacimiento: "",
        genero: "",
        emailValid: false,
        telefonoValid: "",
        codigoPostalValid: false,
        idHubspot:"",
      },
      solicitud: {},
      cotizacion: {},
      cotizacionRc: {},
      servicios: {
        servicioDB: "http://138.197.128.236:8081/ws-autos/servicios"
      },
      prospecto: {
        numero: "",
        correo: "",
        nombre: "",
        sexo: "",
        edad: ""
      },
      productoSolicitud: {
        idProspecto: "",
        idTipoSubRamo: 1,
        datos: ""
      },
      cotizaciones: {
        idProducto: "",
        idPagina: "",
        idMedioDifusion: "",
        idEstadoCotizacion: 1,
        idTipoContacto: 1
      },
      cotizacionAli: {
        idCotizacion: "",
        idSubRamo: "",
        peticion: "",
        respuesta: ""
      },
      solicitudes: {
        idCotizacionAli: "",
        idEmpleado: "",
        idEstadoSolicitud: 1,
        idEtiquetaSolicitud: 1,
        idFlujoSolicitud: 1,
        comentarios: ""
      },
      cotizacion: {}
    },
    actions: {
      getToken() {
        return new Promise((resolve, reject) => {
          getTokenService.search(dbService.tokenData).then(
            resp => {
              if (typeof resp.data == "undefined") {
                this.state.config.accessToken = resp.accessToken;
              } else if (typeof resp.data.accessToken != "undefined") {
                this.state.config.accessToken = resp.data.accessToken;
              }
              localStorage.setItem("authToken", this.state.config.accessToken);
              resolve(resp);
            },
            error => {
              reject(error);
            }
          );
        });
      }
    },
    mutations: {
      validarTokenCore: function (state) {
        try {
          if (process.browser) {
            if (localStorage.getItem("authToken") === null) {
              state.sin_token = true;
              console.log("NO HAY TOKEN...");
            } else {
              console.log("VALIDANDO TOKEN...");
              state.config.accessToken = localStorage.getItem("authToken");
              var tokenSplit = state.config.accessToken.split(".");
              var decodeBytes = atob(tokenSplit[1]);
              var parsead = JSON.parse(decodeBytes);
              var fechaUnix = parsead.exp;
              /*
               * Fecha formateada de unix a fecha normal
               * */
              var expiracion = new Date(fechaUnix * 1000);
              var hoy = new Date(); //fecha actual
              /*
               * Se obtiene el tiempo transcurrido en milisegundos
               * */
              var tiempoTranscurrido = expiracion - hoy;
              /*
               * Se obtienen las horas de diferencia a partir de la conversión de los
               * milisegundos a horas.
               * */
              var horasDiferencia =
                Math.round(
                  (tiempoTranscurrido / 3600000 + Number.EPSILON) * 100
                ) / 100;

              if (hoy > expiracion || horasDiferencia < state.tiempo_minimo) {
                state.sin_token = "expirado";
              }
            }
          }
        } catch (error2) {
          console.log(error2);
        }
      },
      cotizacionPromo: function (state) {
        let aseguradora;
        if (state.config.aseguradora == "AZTECA" || state.config.aseguradora == "GENERICAS") {
          aseguradora = "GNP"
        } else {
          aseguradora = state.config.aseguradora;
        }

        cotizacionPromo
          .search(aseguradora)
          .then(resp => {
            resp = resp.data;
            if(parseInt(resp.discount )){
              state.config.descuento = resp.discount
              }else{
                  state.config.descuento = 0
              }


              if (parseInt(resp.delay)) {
                state.config.time = resp.delay
                } else {
                state.config.time = 5000
                }


            
          }).catch(error=>{
              state.config.descuento = 0;
              state.config.time = resp.delay

          });

        ;
      },
      telApi: function (state) {
        try {
          telApi
            .search(state.config.idMedioDifusion)
            .then(resp => {
              resp = resp.data;
              let telefono = resp.telefono;
              if (parseInt(telefono)) {
                var tel = telefono + ""
                var tel2 = tel.substring(2, tel.length)
                // console.log(tel2);
                state.config.telefonoAS = tel2
              }
            });
        } catch (error) {
          console.log(error);
        }
      },

      saveHubspot:function(state){
        const socios = {
					"AZTECA": "SEGUROS AZTECA",
					"QUALITAS": "QUALITAS",
					"GNP": "GNP",
					"MAPFRE": "MAPFRE"
				}
				let data ={
					mobilephone:  state.formData.telefono,
					email:  state.formData.correo,
					firstname:  state.formData.nombre,
					lastname: "",
					marca:  state.formData.marca,
					modelo:  state.formData.modelo,
					submarca:  state.formData.descripcion,
					socio: socios[state.config.aseguradora]
				};
				saveHub
					.search(
					JSON.stringify(data)
					)
					.then((resp) => {
					 state.formData.idHubspot = resp.data.id;
          this.commit("saveDataCliente")

					})
					.catch((error) => {
					console.log("mensaje de error33 ¨*********",error )
									var status = "ERR_CONNECTION_REFUSED";
									if (typeof error.status != "undefined") {
										status = error.status;
									} else if (typeof error.response != "undefined") {
										if (typeof error.response.status != "undefined") {
											status = error.response.status;
										}
									}
									if (status === 401) {

										this.$store
											.dispatch("getToken")
											.then((resp) => {
												if (typeof resp.data == "undefined") {
													 state.config.accessToken =
														resp.accessToken;
												} else if (
													typeof resp.data.accessToken !=
													"undefined"
												) {
								 state.formData.idHubspot = resp.data.message
								console.log("mensaje de error11 ¨*********", state.formData.idHubspot )

												}
                        this.commit("saveDataCliente")

											})
											.catch((error) => {
												
							 state.formData.idHubspot = resp.data.message

                        this.commit("saveDataCliente")

											});
									} else {
						 state.formData.idHubspot = typeof error.response == 'undefined' ? "{}" : error.response.data.message

                    this.commit("saveDataCliente")

									}
								});
			},

      saveDataCliente: function (state) {
        state.formData.dominioCorreo = state.config.dominioCorreo;

        let peticionProspecto = {}

        let servicio = ''
        if (state.config.aseguradora == "QUALITAS" ){
          servicio = 'MOTOPARTICULAR'
        }else if ( state.config.aseguradora == "GNP") {
          servicio = 'MOTOS'
        }
        // console.log(servicio); 
        // console.log(state.config.tresCaminos);

        if (state.config.tresCaminos) {
          peticionProspecto = {
            aseguradora: state.config.aseguradora,
            clave: state.formData.clave,
            cp: state.formData.cp,
            descripcion: state.formData.descripcion,
            detalle: state.formData.detalle,
            descuento: state.config.descuento,
            edad: state.formData.edad,
            fechaNacimiento: (
              "01/01/" +
              (
                new Date().getFullYear() - state.formData.edad
              ).toString()
            ).toString("dd/MM/yyyyy"),
            genero:
              state.formData.genero,
            marca: state.formData.marca,
            modelo: state.formData.modelo,
            movimiento: "cotizacion",
            paquete: "AMPLIA",
            servicio: servicio,
          };
          state.config.dataPeticionCotizacion = peticionProspecto;
        } else {

          peticionProspecto = {
            nombreUsr: state.formData.nombre,
            telefonoUsr: state.formData.telefono,
            emailUsr: state.formData.correo,
            generoUsr: state.formData.genero,
            codigoPostalUsr: state.formData.cp,
            edadUsr: state.formData.edad,
            respuestaCot: "{}",
            emailValid: state.formData.emailValid === true ? 1 : 0,
            telefonoValid: state.formData.telefonoValid,
            mensajeSMS: state.config.textoSMS,
            codigoPostalValid: state.formData.codigoPostalValid === true ? 1 : 0,
            idSubRamo: state.config.idSubRamo,
            idMedioDifusion: state.config.idMedioDifusion,
            aseguradora: state.config.aseguradora,
            idPagina: state.config.idPagina,
            telefonoAS: state.config.telefonoAS
          };
          state.config.dataPeticionCotizacion = {}
        }

        let datosCliente;
        state.formData.telefonoValid = false;
        datosCliente = {
          datosCot: JSON.stringify(state.formData),
          idMedioDifusion: state.config.idMedioDifusion,
          idPagina: state.config.idPagina,
          idSubRamo: state.config.idSubRamo,  
          peticionesAli: JSON.stringify(peticionProspecto),
        };
        this.commit("enhancedConversionGTAG")
        saveDataCliente
          .search(
            JSON.stringify(datosCliente),
            state.config.accessToken
          )
          .then((resp) => {            
            state.config.idLog_cliente = resp.data.id;
            this.commit("saveDataMoto");

          })
          .catch((error) => {
            var status = "ERR_CONNECTION_REFUSED";
            if (typeof error.status != "undefined") {
              status = error.status;
            } else if (typeof error.response != "undefined") {
              if (typeof error.response.status != "undefined") {
                status = error.response.status;
              }
            }
            if (status === 401) {
              dispatch("getToken")
                .then((resp) => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.config.accessToken = resp.data.accessToken;
                  }
                  this.commit("saveDataCliente")
                })
                .catch((error) => {
                  console.log("Hubo un problema al generar el token");
                  state.idEndPoint = 1;
                  //this.catchStatusAndMessage(error, {tokenData: process.env.tokenData,});

                });
            } else {
              state.idEndPoint = 1;
              //this.catchStatusAndMessage(error, datosCliente);
            }
          });
      },
      enhancedConversionGTAG: function (state) {
        dataLayer.push({
          'event': 'Lead',
          'phone': '52' + state.formData.telefono,
          'email': state.formData.correo,
        });
      },

      validateData: function (state) {
				console.log("tiempo a esperar",  state.config.time)//  es el tiempo definido por el servicio de discount
        this.commit("saveHubspot")

        validacion
          .validarCorreo(state.formData.correo)
          .then(resp => {
            //state.formData.emailValid = resp.data.valido;
            if (typeof resp.data == "undefined") {
              state.formData.emailValid = resp.valido;
            } else if (typeof resp.data.valido != "undefined") {
              state.formData.emailValid = resp.data.valido;
            }
            validacion
              .validarTelefono(state.formData.telefono)
              .then(resp => {
                //state.formData.telefonoValid = resp.data.mensaje
                if (typeof resp.data == "undefined") {
                  state.formData.telefonoValid = resp.mensaje;
                } else if (typeof resp.data.mensaje != "undefined") {
                  state.formData.telefonoValid = resp.data.mensaje;
                }
                this.commit("validacionCP");
              })
              .catch(error => {
                state.formData.telefonoValid = "FIJO";
                state.idEndPoint = 169;
                state.status =
                  error.response !== undefined ? error.response.status : 500;
                state.message =
                  error.response !== undefined
                    ? JSON.stringify({
                      error: error.response.data,
                      peticion: JSON.parse(error.config.data)
                    })
                    : "ERR_CONNECTION_REFUSED";
                this.commit("validacionCP");
              });
          })
          .catch(error => {
            state.formData.emailValid = true;
            state.idEndPoint = 157;
            state.status =
              error.response !== undefined ? error.response.status : 500;
            state.message =
              error.response !== undefined
                ? JSON.stringify({
                  error: error.response.data,
                  peticion: JSON.parse(error.config.data)
                })
                : "ERR_CONNECTION_REFUSED";

            validacion
              .validarTelefono(state.formData.telefono)
              .then(resp => {
                //state.formData.telefonoValid = resp.data.mensaje
                if (typeof resp.data == "undefined") {
                  state.formData.telefonoValid = resp.mensaje;
                } else if (typeof resp.data.mensaje != "undefined") {
                  state.formData.telefonoValid = resp.data.mensaje;
                }
                this.commit("validacionCP");
              })
              .catch(error => {
                state.formData.telefonoValid = "FIJO";
                state.idEndPoint = 169;
                state.status =
                  error.response !== undefined ? error.response.status : 500;
                state.message =
                  error.response !== undefined
                    ? JSON.stringify({
                      error: error.response.data,
                      peticion: JSON.parse(error.config.data)
                    })
                    : "ERR_CONNECTION_REFUSED";

                this.commit("validacionCP");
              });
          });
      },
      validacionCP(state) {
        validacion
          .validarCodigoPostal(
            state.formData.cp,
            state.config.accessToken
          )
          .then(resp => {
            state.formData.codigoPostalValid =
              resp.data !== undefined ? true : false;
              // console.log(state.config.tresCaminos);
              if (state.config.tresCaminos) {
              this.commit("saveProspecto");
            } else {
             //this.commit("saveDataMoto");
            }
          })
          .catch(error => {
            var status = error.response.status
              ? error.response.status
              : "ERR_CONNECTION_REFUSED";
            state.formData.codigoPostalValid = true;
            if (status === 401) {
              this.dispatch("getToken")
                .then(resp => {
                  if (typeof resp.data == "undefined") {
                    state.config.accessToken = resp.accessToken;
                  } else if (typeof resp.data.accessToken != "undefined") {
                    state.config.accessToken = resp.data.accessToken;
                  }
                  //this.commit("validacionCP");
                })
                .catch(error => {
                  state.idEndPoint = 49;
                  state.status =
                    error.response !== undefined ? error.response.status : 500;
                  state.message =
                    error.response !== undefined
                      ? JSON.stringify({
                        error: error.response.data,
                        peticion: JSON.parse(error.config.data)
                      })
                      : "ERR_CONNECTION_REFUSED";

                  if (state.config.tresCaminos) {
                    //this.commit("saveProspecto");
                  } else {
                    //this.commit("saveDataMoto");
                  }
                });
            } else {
              state.idEndPoint = 16;
              state.status =
                status !== "ERR_CONNECTION_REFUSED"
                  ? error.response.status
                  : 500;
              state.message =
                status !== "ERR_CONNECTION_REFUSED"
                  ? JSON.stringify({
                    error: error.response.data,
                    peticion: JSON.parse(error.config.data)
                  })
                  : "ERR_CONNECTION_REFUSED";

              if (state.config.tresCaminos) {
                this.commit("saveProspecto");


              } else {
                this.commit("saveDataMoto");
              }
            }
          });
      },

      saveProspecto: function (state) {
        // Se agrego JSON para enviar peticonAli, logica especial para brandingV3 / updateIdCotizacionAli
        // console.log(state.config.tresCaminos);
        state.formData.idLogData= resp.data.data.idLogClient;
        let dataProspecto;
        dataProspecto = {
          nombreUsr: state.formData.nombre,
          emailUsr: state.formData.correo,
          datosCot: JSON.stringify(state.formData),
          telefonoUsr: state.formData.telefono,
          generoUsr: state.formData.genero,
          codigoPostalUsr: state.formData.cp,
          edadUsr: state.formData.edad,
          idMedioDifusion: state.config.idMedioDifusion,
          idPagina: state.config.idPagina,
          ip: state.config.ipCliente,
          idSubRamo: state.config.idSubRamo,
          peticionAli: JSON.stringify(state.config.dataPeticionCotizacion),
        };
        saveService
          .saveProspecto(
            JSON.stringify(dataProspecto),
            state.config.accessToken
          )
          .then((resp) => {
            /*NUMERO DE COTIZACION DEL CLIENTE
             * idCotizaciones
             * idProductoSolicitud
             * */
            this.state.config.idCotizacionNewCore = resp.data.data.idCotizaciones;
            this.state.config.idProductoSolicitud =
              resp.data.data.idProductoSolicitud;
            this.state.config.cotizacionAli = resp.data.data.idCotizacionAli;
            this.commit("cotizaMoto"); // cotizacion directa

          })
          .catch((error) => {
            var status =
              error.response !== undefined
                ? error.response.status
                : "ERR_CONNECTION_REFUSED";
            if (status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  state.config.accessToken = resp.accessToken;
                  this.commit("saveProspecto");
                })
                .catch((error) => {
                  var status =
                    error.response !== undefined
                      ? error.response.status
                      : "ERR_CONNECTION_REFUSED";
                  state.idEndPoint = 382;
                  state.status =
                    status !== "ERR_CONNECTION_REFUSED" ? status : 500;
                  state.message =
                    status !== "ERR_CONNECTION_REFUSED"
                      ? JSON.stringify({
                        error: error.response.data,
                        peticion: JSON.parse(error.config.data),
                      })
                      : JSON.stringify({
                        error: error,
                        peticion: { tokenData: dbService.tokenData },
                      });
                      this.commit("cotizaMoto");
                });
            } else {
              state.idEndPoint = 382;
              state.status = status !== "ERR_CONNECTION_REFUSED" ? status : 500;
              state.message =
                status !== "ERR_CONNECTION_REFUSED"
                  ? JSON.stringify({
                    error: error.response.data,
                    peticion: JSON.parse(error.config.data),
                  })
                  : JSON.stringify({ error: error, peticion: dataProspecto });
                  this.commit("cotizaMoto");
            }
          });
      },


      cotizaMoto: function (state) {

        let dataMotos = {} //cotizacion motos

        let servicio = ''
        if (state.config.aseguradora == "QUALITAS" ){
          servicio = 'MOTOPARTICULAR'
        }else if ( state.config.aseguradora == "GNP") {
          servicio = 'MOTOS'
        }

        
          dataMotos = {
            aseguradora: state.config.aseguradora,
            clave: state.formData.clave,
            marca: state.formData.marca,
            modelo: state.formData.modelo,
            descripcion: state.formData.descripcion,
            cp: state.formData.cp,
            descuento: state.formData.descuento,
            edad: state.formData.edad,
            fechaNacimiento: state.formData.fechaNacimiento,
            genero: state.formData.genero,
            movimiento: "cotizacion",
            paquete: "AMPLIA",
            servicio: servicio
          };
        
      
        cotizarMoto
          .search(JSON.stringify(dataMotos), state.config.accessToken, state.config.aseguradora)
          .then(resp => {
            resp = JSON.parse(resp.data.data);
            state.cotizacion = resp;
            state.cotizacion.cliente.telefono = state.formData.telefono;
            state.cotizacion.cliente.fechaNacimiento = state.formData.fechaNacimiento;
            // console.log(resp);
            if (typeof resp != 'string') {
              if (
                Object.keys(resp.cotizacion).includes('primaTotal') &&
                resp.cotizacion.primaTotal != null &&
                resp.cotizacion.primaTotal != ''
              ) {
                state.formData.precio = state.cotizacion.cotizacion.primaTotal;
                this.commit("saveCotizacion");  } else {
          
              }
            }
            this.commit("saveCotizacion");
          })
          .catch(error => {

            var status =
              error.response !== undefined
                ? error.response.status
                : "ERR_CONNECTION_REFUSED";
            if (status === 401) {
              this.dispatch("getToken")
                .then(resp => {
                  state.config.accessToken = resp.data.accessToken;
                  this.commit("cotizaMoto");

                })
                .catch(error => {
                  var status =
                    error.response !== undefined
                      ? error.response.status
                      : "ERR_CONNECTION_REFUSED";
                  state.idEndPoint = 1707;
                  state.status =
                    status !== "ERR_CONNECTION_REFUSED" ? status : 500;
                  state.message =
                    status !== "ERR_CONNECTION_REFUSED"
                      ? JSON.stringify({
                        error: error.response.data,
                        peticion: JSON.parse(error.config.data)
                      })
                      : "ERR_CONNECTION_REFUSED";

                  this.commit("saveCotizacion");

                });
            } else {
              state.idEndPoint = 71;
              state.status = status !== "ERR_CONNECTION_REFUSED" ? status : 500;
              state.message =
                status !== "ERR_CONNECTION_REFUSED"
                  ? JSON.stringify({
                    error: error.response.data,
                    peticion: JSON.parse(error.config.data)
                  })
                  : "ERR_CONNECTION_REFUSED";

              this.commit("saveCotizacion");

            }
          });
      },

      saveCotizacion: function (state) {
        this.commit("valid");
        state.config.reload = false
        var peticionAli = JSON.stringify(state.config.dataPeticionCotizacion);
        state.cotizacion =
          typeof state.cotizacion === "string" ? {} : state.cotizacion;
        var jsonPeticionWs = {
          idCotizacion: state.config.idCotizacionNewCore,
          idSubRamo: state.config.idSubRamo,
          peticion: peticionAli,
          respuesta: JSON.stringify(state.cotizacion), //POR ESTRETEGIA AL TARDAR MUCHO EN LA COTIZACION DE GNP
        };

        saveService
          .saveCotizacion(
            JSON.stringify(jsonPeticionWs),
            this.state.config.accessToken,
            state.config.cotizacionAli
          )
          .then((respNuevaCotizacion) => {
            state.config.id_cotizacion_a = respNuevaCotizacion;

            this.state.config.cargandocotizacion = true;
          })
          .catch((error) => {
            var status =
              error.response !== undefined
                ? error.response.status
                : "ERR_CONNECTION_REFUSED";
            if (status === 401) {
              this.dispatch("getToken")
                .then((resp) => {
                  state.config.accessToken = resp.accessToken;
                  return this.commit("saveCotizacion");
                })
                .catch((error) => {
                  var status =
                    error.response !== undefined
                      ? error.response.status
                      : "ERR_CONNECTION_REFUSED";
                  state.idEndPoint = 382;
                  state.status =
                    status !== "ERR_CONNECTION_REFUSED" ? status : 500;
                  state.message =
                    status !== "ERR_CONNECTION_REFUSED"
                      ? JSON.stringify({
                        error: error.response.data,
                        peticion: JSON.parse(error),
                      })
                      : JSON.stringify({
                        error: error,
                        peticion: { tokenData: dbService.tokenData },
                      });
                  this.commit("valid");
                  this.state.config.cargandocotizacion = true;
                });
            } else {
              state.idEndPoint = 382;
              state.status = status !== "ERR_CONNECTION_REFUSED" ? status : 500;
              state.message =
                status !== "ERR_CONNECTION_REFUSED"
                  ? JSON.stringify({
                    error: error.response.data,
                    peticion: JSON.parse(error),
                  })
                  : JSON.stringify({ error: error, peticion: jsonPeticionWs });
              this.commit("valid");
              this.state.config.cargandocotizacion = true;
            }
          });
      },

      saveDataMoto: function (state) {
        this.commit("valid");

        /*Se designo ekomi con el idMedioDifusion para diferenciarlo y que no guarde en el CRM*/
        if (state.config.idMedioDifusion !== 1) {
          database
            .search(
              state.formData.nombre,
              state.formData.telefono,
              state.formData.correo,
              state.formData.genero,
              state.formData.cp,
              state.formData.edad,
              JSON.stringify(state.formData),
              state.config.cotizacion === true
                ? JSON.stringify(state.cotizacion)
                : "{}",
              state.formData.emailValid == true ? 1 : 0,
              state.formData.telefonoValid,
              state.config.textoSMS,
              state.formData.codigoPostalValid == true ? 1 : 0,
              state.config.idSubRamo,
              state.config.idMedioDifusion,
              state.formData.aseguradora,
              state.config.idPagina,
              state.config.ipCliente,
              state.config.telefonoAS,
              state.config.accessToken
            )
            .then(resp => {
              state.ejecutivo.nombre = resp.data.nombreEjecutivo;
              state.ejecutivo.correo = resp.data.correoEjecutivo;
              state.cargandocotizacion = true;
              console.log("entraaaaaaaaaaaaa" , resp)

            })
            .catch(error => {
              console.log("entraaaaaaaaaaaaa error")
              state.msj = true;
              state.config.loading = false;
              // console.log("error saveData", error);
              var status =
                error.response !== undefined
                  ? error.response.status
                  : "ERR_CONNECTION_REFUSED";
              if (status === 401) {
                this.dispatch("getToken")
                  .then(resp => {
                    state.config.accessToken = resp.data.accessToken;
                    this.commit("sa veDataMoto");
                  })
                  .catch(error => {
                    var status =
                      error.response !== undefined
                        ? error.response.status
                        : "ERR_CONNECTION_REFUSED";
                    state.idEndPoint = 1707;
                    state.status =
                      status !== "ERR_CONNECTION_REFUSED" ? status : 500;
                    state.message =
                      status !== "ERR_CONNECTION_REFUSED"
                        ? JSON.stringify({
                          error: error.response.data,
                          peticion: JSON.parse(error.config.data)
                        })
                        : "ERR_CONNECTION_REFUSED";

                        console.log("ultimooooantes")

                  });
              } else {
                state.msj = true;
                state.config.loading = false;
  
                state.idEndPoint = 84;
                state.status =
                  status !== "ERR_CONNECTION_REFUSED" ? status : 500;
                state.message =
                  status !== "ERR_CONNECTION_REFUSED"
                    ? JSON.stringify({
                      error: error.response.data,
                      peticion: JSON.parse(error.config.data)
                    })
                    : "ERR_CONNECTION_REFUSED";
              }
            });
        } else {
          state.config.loading = false;

          state.msj = true;
        }
      },

      valid: function (state) {
        console.log("Validando datos de la cotizacion");
        if (
          state.formData.precio === "" &&
          state.ejecutivo.id === ""
        ) {
          state.msj = true;
        }

        if (
          state.formData.precio == 0.0 ||
          state.formData.precio === "" ||
          state.formData.precio <= 1000 ||
          !state.formData.precio
        ) {
          state.config.msjEje = true;
          state.config.loading = false;
        } else {
          state.cargandocotizacion = true;
        }
      },

      promociones(state) {
        var fecha = new Date();
        var dia = fecha.getDate();
        // var dia = 24;
        // console.log("dia:" + dia);
        // console.log('descuento'+ descuentoPagina)
        for (var i = 0; i < promo.all.length; i++) {
          //Recorre las aseguradoras
          if (promo.all[i].aseguradorapromo == state.config.aseguradorapromo) {
            // console.log('aseguradorapromo:'+promo.all[i].aseguradorapromo);
            for (
              var j = 0;
              j < promo.all[i].promos.length;
              j++ //Recorre las promos
            ) {
              for (
                var k = 0;
                k < promo.all[i].promos[j].dias.length;
                k++ //Recorre los dias
              ) {
                if (promo.all[i].promos[j].dias[k] == dia) {
                  state.config.descuentoPagina =
                    promo.all[i].promos[j].descuentoPagina;
                  state.config.promoMsi = promo.all[i].promos[j].promoMsi;
                  state.config.nombrepromo = promo.all[i].promos[j].nombre;
                  // console.log(state.config.nombrepromo);
                } else {
                }
              } //Fin for dias
            } //Fin for promos
          }
        } //Fin for aseguradoras
      },

      limpiarCoberturas(state) {
        if (state.cotizacion.Coberturas) {
          for (var c in state.cotizacion.Coberturas[0]) {
            if (state.cotizacion.Coberturas[0][c] === "-") {
              delete state.cotizacion.Coberturas[0][c];
            }
          }
        }
      }
    }
  });
};
export default createStore;
